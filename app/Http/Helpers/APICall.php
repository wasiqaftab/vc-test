<?php

namespace App\Http\Helpers;

class APICall  {

	public $data;
	public $url;
	public $method;

    public function setData($data) {
		$this->data = $data;
	}
	public function setUrl($url) {
		$this->url = $url;
	}
	public function setMethod($method) {
		$this->method = $method;
	}
	public function getData(){
		return $this->data;
	}
	public function getUrl(){
		return $this->url;
	}
	public function getMethod(){
		return $this->method;
	}

    /**
     * @param $method
     * @param $url
     * @param $data
     *
     * @return bool|string
     */
    public function CallAPI($method, $url, $data) {
		$curl = curl_init();
		$this->setMethod($method);
		$this->setData($data);
		$this->setUrl($url);

		switch ($this->getMethod())
		{
			case 'POST':
			curl_setopt($curl, CURLOPT_POST, 1);

			if ($this->getData())
				curl_setopt($curl, CURLOPT_POSTFIELDS, $this->getData());
			break;
			case 'PUT':
			curl_setopt($curl, CURLOPT_PUT, 1);
			break;
			default:
			if ($this->getData())
				$url = sprintf("%s?%s", $this->getUrl(), http_build_query($this->getData()));
		}

		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($curl, CURLOPT_USERPWD, 'username:password');

		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

		$result = curl_exec($curl);

		curl_close($curl);

		return $result;
	}
	public function fetchResult($response): string
    {

		$dataArray = json_decode($response, TRUE);
		$a = preg_split('#([0-9]),([0-9])#i', $dataArray['message']);
		// print_r($dataArray['message']);
		if($dataArray['message'] === 'Lost contact.') {
			$status = '410';
		}
		elseif ($dataArray['message'] === 'Destination reached.') {
			$status = '200';
		}
		else {
			$status = '417';
		}
		return $status;

	}
}
