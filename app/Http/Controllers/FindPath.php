<?php


namespace App\Http\Controllers;


use App\Http\Helpers\APICall;

class FindPath extends Controller
{

    private $apiUrl="https://deathstar.dev-tests.vp-ops.com/empire.php";

    public function findPath(): void
    {
        $apiCall = new APICall();


        $data = array( "name" => "test", "path" => "");

        $initialX = 0;
        $initialY = 4;
        $responseCode = 410;
        $path = "";
        $possibleMoves[0] = array('f', 'l', 'r');
        $possibleMoves[1] = array('f', 'r', 'l');
        $possibleMoves[2] = array('l', 'f', 'r');
        $possibleMoves[3] = array('l', 'r', 'f');
        $possibleMoves[4] = array('r', 'f', 'l');
        $possibleMoves[5] = array('r', 'l', 'f');


        for ($a=0, $aMax = count($possibleMoves[$a]); $a < $aMax; $a++) {
            // $b = 0;
            while ($responseCode != 200) {
                for ($i = 0, $iMax = count($possibleMoves[$i]); $i < $iMax; $i++) {
                    if ($possibleMoves[$a][$i] === 'l') {
                        --$initialY;
                        $newPath = $path.$possibleMoves[$a][$i];
                        $data['path'] = $newPath;
                        $result = $apiCall->CallAPI('GET', $this->apiUrl, $data);
                        $responseCode = $apiCall->fetchResult($result);
                        if($responseCode === '417') {
                            ++$initialY;
                            $data['path'] = $path;
                        }
                    }
                    elseif ($possibleMoves[$a][$i] === 'r') {
                        ++$initialY;
                        $newPath = $path.$possibleMoves[$a][$i];
                        $data['path'] = $newPath;
                        $result = $apiCall->CallAPI('GET', $this->apiUrl, $data);
                        $responseCode = $apiCall->fetchResult($result);
                        if($responseCode == '417') {
                            --$initialY;
                            $data['path'] = $path;
                        }
                    }
                    elseif ($possibleMoves[$a][$i] === 'f') {
                        --$initialY;
                        $newPath = $path.$possibleMoves[$a][$i];
                        $data['path'] = $newPath;
                        $result = $apiCall->CallAPI('GET', $this->apiUrl, $data);
                        $responseCode = $apiCall->fetchResult($result);
                        if($responseCode === '417') {
                            ++$initialX;
                            $data['path'] = $path;
                        }
                    }
                    $path = $data['path'];
                }
                if($responseCode == "417") {
                    break;
                }

                echo "Path is: ".$data['path']."<br>";
                // $b++;
            }
        }
    }


}
